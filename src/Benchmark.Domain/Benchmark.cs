﻿namespace Benchmark.Domain
{
    public class Benchmark
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Action { get; set; }
        public double ElapsedMilliseconds { get; set; }
    }
}