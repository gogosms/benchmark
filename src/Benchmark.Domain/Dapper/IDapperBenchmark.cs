﻿namespace Benchmark.Domain.Dapper
{
    public interface IDapperBenchmark: IBenchmark
    {
        Benchmark [] GetAllBenchmark();
    }
}