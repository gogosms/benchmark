﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Dapper;

namespace Benchmark.Domain.Dapper
{
    public class DapperBenchmark : IDapperBenchmark
    {
        public string Name { get; } = "Dapper";

        public void Save(Benchmark benchmark)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                conn.Execute(@"INSERT INTO [dbo].[Benchmark] ([Name],[Action],[ElapsedMilliseconds]) 
                    VALUES(@Name, @Action, @ElapsedMilliseconds)", benchmark);
            }
        }

        private static string ConnectionString = "Data Source=.;Initial Catalog=BenchmarkDB;Integrated Security=True";
        
        public (Employee employee, double elapsedMilliseconds) Get(int id)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                var stop = new Stopwatch();
                stop.Start();
                conn.Open();
                var employee = conn.Query<Employee>("select * from Employee where Id = @Id", new { Id = id }).FirstOrDefault();
                stop.Stop();
                return (employee, stop.ElapsedMilliseconds);
            }
        }

        public double Save(Employee employee)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                var stop = new Stopwatch();
                stop.Start();
                conn.Open();
                conn.Execute("INSERT INTO [dbo].[Employee]([Names],[LastName]) VALUES (@Names,@LastName)", employee);
                stop.Stop();
                return stop.ElapsedMilliseconds;
            }
        }

        public (Employee[] employees, double elapsedMilliseconds) GetAll()
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                var stop = new Stopwatch();
                stop.Start();
                conn.Open();
                var employes = conn.Query<Employee>("select * from Employee").ToArray();
                stop.Stop();
                return (employes, stop.ElapsedMilliseconds);
            }
        }

        public Benchmark [] GetAllBenchmark()
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                return conn.Query<Benchmark>("select * from Benchmark").ToArray();
     
            }
        }

    }
}
