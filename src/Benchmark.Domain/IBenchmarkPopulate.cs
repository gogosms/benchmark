﻿namespace Benchmark.Domain
{
    public interface IBenchmarkPopulate
    {
        Benchmark[] GetBenchmarks(bool cleanDb);
    }
}