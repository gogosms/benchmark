﻿namespace Benchmark.Domain
{
    public class Employee
    {
        public int Id { get; set; }
        public string Names { get; set; }
        public string LastName { get; set; }
    }
}