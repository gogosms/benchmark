﻿namespace Benchmark.Domain
{
    public interface IBenchmark
    {
        (Employee employee, double elapsedMilliseconds) Get(int id);
        double Save(Employee employee);
        (Employee[] employees, double elapsedMilliseconds) GetAll();

        string Name { get; }

        void Save(Benchmark benchmark);
    }
}
