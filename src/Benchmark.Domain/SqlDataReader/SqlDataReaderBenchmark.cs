﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Benchmark.Domain.SqlDataReader
{
    public class SqlDataReaderBenchmark : ISqlDataReaderBenchmark
    {
        private static string ConnectionString = "Data Source=.;Initial Catalog=BenchmarkDB;Integrated Security=True";

        public string Name { get; } = "SqlDataReader";

        public void Save(Benchmark benchmark)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (var command = new SqlCommand(
                    @"INSERT INTO [dbo].[Benchmark] ([Name],[Action],[ElapsedMilliseconds])
                            VALUES (@Name, @Action, @ElapsedMilliseconds)", con))
                {
                    var name = new SqlParameter("@Name", SqlDbType.NVarChar) { Value = benchmark.Name};
                    var action = new SqlParameter("@Action", SqlDbType.NVarChar) { Value = benchmark.Action};
                    var elapsedMilliseconds = new SqlParameter("@ElapsedMilliseconds", SqlDbType.Float)
                        { Value = benchmark.ElapsedMilliseconds };

                    command.Parameters.Add(name);
                    command.Parameters.Add(action);
                    command.Parameters.Add(elapsedMilliseconds);
                    command.ExecuteNonQuery();
                    
                }

            }
        }

        public void CleanDb()
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (var command = new SqlCommand($"Delete from {nameof(Employee)}; Truncate Table {nameof(Employee)}", con))
                    command.ExecuteNonQuery();

                using (var command = new SqlCommand($"Delete from {nameof(Benchmark)}; Truncate Table {nameof(Benchmark)}", con))
                    command.ExecuteNonQuery();

            }
        }

        public (Employee employee, double elapsedMilliseconds) Get(int id)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                using (var command = new SqlCommand("select * from Employee where Id = @Id", con))
                {
                    var sqlParameter = new SqlParameter("@Id", SqlDbType.Int) { Value = id };
                    command.Parameters.Add(sqlParameter);
                    using (var reader = command.ExecuteReader())
                    {
                        var employee = new Employee();
                        while (reader.Read())
                        {
                            employee.Id = reader.GetInt32(0);
                            employee.Names = reader.GetString(1);
                            employee.LastName = reader.GetString(2);
                        }
                        stopwatch.Stop();
                        return (employee, stopwatch.ElapsedMilliseconds);
                    }
                }
            }
        }

        public double Save(Employee employee)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                using (var command = new SqlCommand(
                    "INSERT INTO [dbo].[Employee]([Names],[LastName]) VALUES (@Names,@LastName)",
                    con))
                {
                    var name = new SqlParameter("@Names", SqlDbType.NVarChar){Value = employee.Names};
                    var lastName = new SqlParameter("@LastName", SqlDbType.NVarChar) { Value = employee.LastName };
                    command.Parameters.Add(name);
                    command.Parameters.Add(lastName);
                    command.ExecuteNonQuery();
                    stopwatch.Stop();
                    return stopwatch.ElapsedMilliseconds;
                }
    
            }
        }

        public (Employee[] employees, double elapsedMilliseconds) GetAll()
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                using (var command = new SqlCommand("select * from Employee", con))
                    using (var reader = command.ExecuteReader())
                    {
                        var employees = new List<Employee>();
                        while (reader.Read())
                        {
                            var employee = new Employee
                            {
                                Id = reader.GetInt32(0),
                                Names = reader.GetString(1),
                                LastName = reader.GetString(2)
                            };
                            employees.Add(employee);
                        }
                        stopwatch.Stop();
                        return (employees.ToArray(), stopwatch.ElapsedMilliseconds);
                }
         
            }
        }


    }
}
