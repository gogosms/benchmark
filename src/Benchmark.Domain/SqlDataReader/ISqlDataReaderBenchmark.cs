﻿namespace Benchmark.Domain.SqlDataReader
{
    public interface ISqlDataReaderBenchmark : IBenchmark
    {
        void CleanDb();
    }
}