﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Benchmark.Domain.Dapper;
using Benchmark.Domain.SqlDataReader;

namespace Benchmark.Domain
{
    public class BenchmarkPopulate : IBenchmarkPopulate
    {
        private readonly IEnumerable<IBenchmark> _benchmarksProviders;

        public BenchmarkPopulate(IEnumerable<IBenchmark> benchmarksProviders)
        {
            _benchmarksProviders = benchmarksProviders;
        }

        public Benchmark[] GetBenchmarks(bool cleanDb = false)
        {
            // 1.- clean 
            if (cleanDb)
            {
                var sqlReader =  _benchmarksProviders.FirstOrDefault(s =>
                    s.Name.Equals("SqlDataReader",  StringComparison.InvariantCultureIgnoreCase));
                if (sqlReader is ISqlDataReaderBenchmark sqlDataReaderBenchmark)
                {
                    sqlDataReaderBenchmark.CleanDb();
                }
            }

            foreach (var provider in _benchmarksProviders)
            {
                InsertData(provider);
            }

            foreach (var provider in _benchmarksProviders)
            {
                GetAllData(provider);
            }

            foreach (var provider in _benchmarksProviders)
            {
                GetEmployeeData(provider);
            }

            var dapper = _benchmarksProviders.FirstOrDefault(p =>
                p.Name.Equals("Dapper", StringComparison.InvariantCultureIgnoreCase)) as IDapperBenchmark;
            Debug.Assert(dapper != null);
            return dapper.GetAllBenchmark();
        }

        private void GetEmployeeData(IBenchmark provider)
        {
            var elapsedMilliseconds = provider.Get(101).elapsedMilliseconds;
            var benchmark = new Benchmark
            {
                Action = $"Get Employee: '{101}' - {provider.Name}",
                ElapsedMilliseconds = elapsedMilliseconds,
                Name = provider.Name
            };

            provider.Save(benchmark);
        }

        private void GetAllData(IBenchmark provider)
        {
            var elapsedMilliseconds = provider.GetAll().elapsedMilliseconds;
            var benchmark = new Benchmark
            {
                Action = $"GetAll - {provider.Name}",
                ElapsedMilliseconds = elapsedMilliseconds,
                Name = provider.Name
            };

            provider.Save(benchmark);
        }

        private static void InsertData(IBenchmark provider)
        {
            double elapsedMilliseconds = 0;
            var length = 10000;
            for (var i = 0; i < length; i++)
            {
                var employee = new Employee
                {
                    LastName = $"LastName{i} - {provider.Name}",
                    Names = $"Name{i} - {provider.Name}"
                };
                elapsedMilliseconds += provider.Save(employee);
            }

            var benchmark = new Benchmark
            {
                Action = $"Save - {length} records",
                ElapsedMilliseconds = elapsedMilliseconds, Name = provider.Name
            };

            provider.Save(benchmark);

        }
    }
}