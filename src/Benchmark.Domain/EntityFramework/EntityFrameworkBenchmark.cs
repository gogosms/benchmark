﻿using System;
using System.Diagnostics;
using System.Linq;

namespace Benchmark.Domain.EntityFramework
{
    public class EntityFrameworkBenchmark : IEntityFrameworkBenchmark
    {
        public string Name { get; } = "EntityFramework";
        public void Save(Benchmark benchmark)
        {
            using (var dbContext = new BenchmarkDbContext())
            {
                dbContext.Add(benchmark);
                dbContext.SaveChanges();
            }
        }

        public (Employee employee, double elapsedMilliseconds) Get(int id)
        {
            var stop = new Stopwatch();
            stop.Start();
            using (var dbContext = new BenchmarkDbContext())
            {
                var employee = dbContext.Employee.FirstOrDefault(e => e.Id.Equals(id));
                stop.Stop();
                return (employee, stop.ElapsedMilliseconds);
            }
        }

        public double Save(Employee employee)
        {
            var stop = new Stopwatch();
            stop.Start();
            using (var dbContext = new BenchmarkDbContext())
            {
                dbContext.Add(employee);
                dbContext.SaveChanges();
                stop.Stop();
                return stop.ElapsedMilliseconds;
            }
        }

        public (Employee[] employees, double elapsedMilliseconds) GetAll()
        {
            var stop = new Stopwatch();
            stop.Start();
            using (var dbContext = new BenchmarkDbContext())
            {
                var employees = dbContext.Employee.ToArray();
                stop.Stop();
                return (employees, stop.ElapsedMilliseconds);
            }
        }

        
    }
}
