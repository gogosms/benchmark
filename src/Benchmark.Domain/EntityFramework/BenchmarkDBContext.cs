﻿using Microsoft.EntityFrameworkCore;

namespace Benchmark.Domain.EntityFramework
{
    public partial class BenchmarkDbContext : DbContext
    {
        public BenchmarkDbContext()
        {
        }

        public BenchmarkDbContext(DbContextOptions<BenchmarkDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Benchmark> Benchmark { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=.;Initial Catalog=BenchmarkDB;Integrated Security=SSPI;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Names).IsRequired();
            });

            modelBuilder.Entity<Benchmark>(entity =>
            {
                entity.Property(e => e.Name).IsRequired().HasMaxLength(50);
                entity.Property(e => e.Action).IsRequired().HasMaxLength(50);
                entity.Property(e => e.ElapsedMilliseconds).IsRequired();
                
            });
        }
    }
}
