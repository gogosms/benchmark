using Benchmark.Domain;
using Benchmark.Domain.SqlDataReader;
using NUnit.Framework;

namespace Benchmark.NUnitTest
{
    [TestFixture]
    public class SqlDataReaderBenchmarkTest
    {
        private SqlDataReaderBenchmark _dataReaderBenchmark;
        [SetUp]
        public void Setup()
        {
            _dataReaderBenchmark = new SqlDataReaderBenchmark();
        }
        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        public void Must_SqlDataReader_GetAll()
        {
            //expected
            var result = _dataReaderBenchmark.GetAll();
            Assert.IsNotNull(result);
            CollectionAssert.IsNotEmpty(result.employees);
        }

        [Test]
        public void Must_SqlDataReader_Save()
        {
            //expected
            Assert.DoesNotThrow(() => _dataReaderBenchmark.Save(new Employee { LastName = "Valdez", Names = "Rodrigo" }));
            
        }
    }
}