using Benchmark.Domain;
using Benchmark.Domain.Dapper;
using NUnit.Framework;

namespace Benchmark.NUnitTest
{
    [TestFixture]
    public class BenchmarkDapperTests
    {
        private DapperBenchmark _dapperBenchmark;
        
        [SetUp]
        public void Setup()
        {
            _dapperBenchmark = new DapperBenchmark();
        }
        
        [TearDown]
        public void TearDown()
        {

        }


        [Test]
        public void Must_BenchmarkDapper()
        {
            //expected
            var result = _dapperBenchmark.GetAll();
            Assert.IsNotNull(result);
            CollectionAssert.IsNotEmpty(result.employees);
        }

        [Test]
        public void Must_BenchmarkDapper_Save()
        {
            //expected
            Assert.DoesNotThrow(() => _dapperBenchmark.Save(new Employee { LastName = "Valdez", Names = "Rodrigo" }));

        }
    }
}