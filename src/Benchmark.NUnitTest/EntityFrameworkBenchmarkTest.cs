using Benchmark.Domain;
using Benchmark.Domain.EntityFramework;
using NUnit.Framework;

namespace Benchmark.NUnitTest
{
    [TestFixture]
    public class EntityFrameworkBenchmarkTest
    {
        private EntityFrameworkBenchmark _entityFrameworkBenchmark;
        [SetUp]
        public void Setup()
        {
            _entityFrameworkBenchmark = new EntityFrameworkBenchmark();
        }

        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        public void Must_BenchmarkDapper()
        {
            //expected
            var result = _entityFrameworkBenchmark.GetAll();
            Assert.IsNotNull(result);
            CollectionAssert.IsNotEmpty(result.employees);
     
        }

        [Test]
        public void Must_EntityFramework_Save()
        {
            //expected
            Assert.DoesNotThrow(() => _entityFrameworkBenchmark.Save(new Employee { LastName = "Valdez", Names = "Rodrigo" }));

        }
    }
}