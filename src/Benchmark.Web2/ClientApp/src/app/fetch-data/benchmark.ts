export interface Benchmark {
  Name: string;
  Action: string;
  ElapsedMilliseconds: string;
}

