import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Benchmark } from './Benchmark';


@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {

  public benchmarks : Benchmark[];

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    http.get<Benchmark[]>(baseUrl + 'api/SampleData/GetBenchmark').subscribe(result => {
      this.benchmarks = result;
    });
  }


}


