export interface Employee {
  id: number;
  names: string;
  lastName: string;
}
