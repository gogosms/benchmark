using System.Collections.Generic;
using Benchmark.Domain;
using Microsoft.AspNetCore.Mvc;

namespace Benchmark.Web.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private readonly IBenchmarkPopulate _benchmarkPopulate;
        public SampleDataController(IBenchmarkPopulate benchmarkPopulate)
        {
            _benchmarkPopulate = benchmarkPopulate;
        }
        
        //[HttpGet("[action]/{id}")]
        //public IEnumerable<string> GetMessage(string id)
        //{
        //    return new[] { $"Test{id}" };
        //}

        //[HttpGet("[action]/{id}")]
        //public IActionResult GetEmployee(int id)
        //{
        //    var employee = _dapperBenchmark.Get(id);
        //    return Json(employee.employee);
        //}

        //[HttpGet("[action]")]
        //public IActionResult GetAllEmployee()
        //{
        //    var employee = _dapperBenchmark.GetAll();
        //    return Json(new { employee.employees, employee.elapsedMilliseconds });
        //}

        [HttpGet("[action]")]
        public IEnumerable<Domain.Benchmark> GetBenchmark()
        {
            return _benchmarkPopulate.GetBenchmarks(true);

        }
    }
}
